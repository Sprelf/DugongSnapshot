package com.sprelf.dugongsnapshot;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SplashActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Test if a username has been set (roundabout way of testing if this is the first time
        // the app has been launched).  If no name has been set, let the splash screen persist
        // longer.
        SharedPreferences settings = getSharedPreferences(DugongSnapshot.SHAREDPREFS_FILE, 0);
        String username = settings.getString(DugongSnapshot.USERNAME_KEY, null);

        int duration = (username == null || username.equals("")) ? 6 : 3;

        Executors.newScheduledThreadPool(1).schedule(new Runnable() {
            @Override
            public void run()
            {
                startActivity(new Intent(getApplicationContext(), CameraActivity.class));
            }
        }, duration, TimeUnit.SECONDS);
    }
}

